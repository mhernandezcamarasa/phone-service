import com.phoneendpoint.entities.PhoneList;
import com.phoneendpoint.services.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;



@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.phoneendpoint.services" })
public class PhoneEndpoint {

    private PhoneService phoneService;

    @Autowired
    public void setPhoneService(final PhoneService phoneService) {
        this.phoneService = phoneService;
    }


    @RequestMapping(value = "/phone/getList", method = RequestMethod.GET)
    public ResponseEntity<PhoneList> retrievePhoneList() {
        ResponseEntity<PhoneList> result = new ResponseEntity<>(HttpStatus.PRECONDITION_REQUIRED);
        PhoneList phoneList = this.phoneService.retrievePhoneList();
        if (!CollectionUtils.isEmpty(phoneList.getPhoneList())) {
            result = new ResponseEntity<>(phoneList, HttpStatus.OK);
        }
        return result;
    }


    public static void main(String[] args) {
        SpringApplication.run(PhoneEndpoint.class, args);
    }

}