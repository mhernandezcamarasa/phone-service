package com.phoneendpoint.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * Representation of phone data
 */
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Phone implements Serializable {

    private static final long serialVersionUID = 638808919338124787L;
    private String phoneId;
    private String image;
    private String name;
    private String description;
    private BigDecimal price;

}
