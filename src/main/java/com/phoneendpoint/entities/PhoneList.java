package com.phoneendpoint.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * Representation of phone data
 */
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhoneList implements Serializable {

    private static final long serialVersionUID = 638808919338124787L;
    private List<Phone> phoneList;


}
