package com.phoneendpoint.services;

import com.phoneendpoint.entities.Phone;
import com.phoneendpoint.entities.PhoneList;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Contains services for phones.
 */
@Service
public class PhoneService {




    public PhoneList retrievePhoneList() {
        return createPhoneDatabase();
    }



    private PhoneList createPhoneDatabase () {

        Phone phone1 = Phone.builder().phoneId("0001").image("http://mydomain/phone1Image.png").name("IphoneX").
                description("Expensive Phone").price(new BigDecimal(1000)).build();

        Phone phone2 = Phone.builder().phoneId("0002").image("http://mydomain/phone2Image.png").name("Samsung S8").
                description("Another Expensive Phone").price(new BigDecimal(800)).build();

        Phone phone3 = Phone.builder().phoneId("0003").image("http://mydomain/phone3Image.png").name("Xiaomi Redmi 3").
                description("Cheap Phone").price(new BigDecimal(150)).build();

        Phone phone4 = Phone.builder().phoneId("0004").image("http://mydomain/phone4Image.png").name("Samsung J5").
                description("Another Cheap Phone").price(new BigDecimal(180)).build();

        List<Phone> phoneList1 = new ArrayList<>();

        phoneList1.add(phone1);
        phoneList1.add(phone2);
        phoneList1.add(phone3);
        phoneList1.add(phone4);



        return PhoneList.builder().phoneList(phoneList1).build();

    }


}
