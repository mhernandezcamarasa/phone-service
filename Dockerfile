FROM openjdk:10-jre-slim
COPY ./target/phoneService-LATEST-SNAPSHOT.jar /usr/src/phoneService/
WORKDIR /usr/src/phoneService
EXPOSE 8080
CMD ["java", "-jar", "phoneService-LATEST-SNAPSHOT.jar"]