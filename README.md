** Deployment instructions

Provided latest releases of maven and docker are installed, and also that we are in the ROOT directory of the project:

1. mvn clean install (In order to generate phoneService-LATEST-SNAPSHOT.jar)

2. docker build -t phoneservice-manual-build . (In order to create docker image)

3. docker run -p 8001:8080 phoneservice-manual-build (In order to start docker container)

4. Once in the browser, type the following to get the phone list : http://localhost:8001/phone/getList , which will
   retrieve a JSON response with the specified list.